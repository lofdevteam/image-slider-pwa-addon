import React from 'react';
import App from '../components/App/index';

export const useImageSlider = (props = {}) => {
    return {
        data: null,
        components: <App props={props} />
    };
};
