import React from 'react';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';
import { useQuery } from '@apollo/client';
import { GET_BANNERS_SLIDER, GET_STORE_CONFIG_BANNER_SLIDER } from './BannerSlider.gql';
import LoadingIndicator from "@magento/venia-ui/lib/components/LoadingIndicator";
import RichContent from "@magento/venia-ui/lib/components/RichContent";
import styles from './style.css';

const Banner = (props) => {
    const { defaultBannerId, baseSecureMediaUrl } = props;
    const { loading: bannerConfigLoading, error: bannerConfigError, data: bannerConfigData } = useQuery(GET_STORE_CONFIG_BANNER_SLIDER)

    if (bannerConfigLoading) return (
        <LoadingIndicator />
    )

    if (!bannerConfigData || !bannerConfigData.storeConfig || !bannerConfigData.storeConfig.lof_bannerslider || (bannerConfigData.storeConfig.lof_bannerslider.enabled === false))
        return (<div/>)
    

    const { loading, error, data } = useQuery(GET_BANNERS_SLIDER, {
        variables: {
            sliderId: defaultBannerId || bannerConfigData.storeConfig.lof_bannerslider.default_homepage_slider
        }
    })
    if (loading) return (
        <LoadingIndicator />
    )
    if (error) {
        console.log("ERROR", error)
        return null
    }
    if (!data) {
        return (<div/>)
    }

    const items = data.lofBannerSlider.banners.map((banner, index) => {
        if (banner.resource_type === "youtube_video") {
            return (
                <div key={index}>
                    <iframe
                        src={banner.resource_path}
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen="1"
                        style={{maxWidth: banner.resource_map.max_width, minWidth: banner.resource_map.min_width, minHeight: 200}}
                    />
                </div>
            )
        }
        else if (banner.resource_type === "external_image") {
            if (banner.link) {
                return (
                    <div key={index}>
                        <a href={`${banner.link}`}>
                            <img
                                className={styles.bannerImage}
                                src={banner.resource_path}
                                style={{maxWidth: banner.resource_map.max_width, minWidth: banner.resource_map.min_width}}
                            />
                            <p className={styles.bannerTitle}>{banner.title}</p>
                        </a>
                    </div>
                )
            }
            else {
                return (
                    <div key={index}>
                        <img
                            className={styles.bannerImage}
                            src={banner.resource_path}
                            style={{maxWidth: banner.resource_map.max_width, minWidth: banner.resource_map.min_width}}
                        />
                        <p className={styles.bannerTitle}>{banner.title}</p>
                    </div>
                )
            }
        }
        else if (banner.resource_type === "local_image") {
            if (banner.link) {
                return (
                    <div key={index}>
                        <a href={`${banner.link}`}>
                            <img
                                className={styles.bannerImage}
                                src={`${baseSecureMediaUrl || bannerConfigData.storeConfig.secure_base_media_url||"http://magento2.landofcoder.com/media/"}${banner.resource_path}`}
                                style={{maxWidth: banner.resource_map.max_width, minWidth: banner.resource_map.min_width}}
                            />
                            <p className={styles.bannerTitle}>{banner.title}</p>
                        </a>
                    </div>
                )
            }
            else {
                return (
                    <div key={index}>
                        <img
                            className={styles.bannerImage}
                            src={`${baseSecureMediaUrl ||bannerConfigData.storeConfig.secure_base_media_url||"http://magento2.landofcoder.com/media/"}${banner.resource_path}`}
                            style={{maxWidth: banner.resource_map.max_width, minWidth: banner.resource_map.min_width}}
                        />
                        <p className={styles.bannerTitle}>{banner.title}</p>
                    </div>
                )
            }
        }
        else {
            return (
                <div key={index} className={styles.bannerImage} style={{maxWidth: banner.resource_map.max_width, minWidth: banner.resource_map.min_width}}>
                    <RichContent html={banner.resource_path}/>
                </div>
            )
        }
    })
    return (
        <Carousel 
            showIndicators={bannerConfigData.storeConfig.lof_bannerslider.show_dots} 
            swipeable={true} 
            showArrows={bannerConfigData.storeConfig.lof_bannerslider.show_nav || false} 
            showStatus={false} 
            showThumbs={false} 
            autoPlay={bannerConfigData.storeConfig.lof_bannerslider.autoplay || false} 
            transitionTime={bannerConfigData.storeConfig.lof_bannerslider.sliding_speed || 1000} 
            infiniteLoop 
            stopOnHover
            >
            {items}
        </Carousel >
    )
}
export default Banner;
