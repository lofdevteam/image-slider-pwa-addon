import { gql } from '@apollo/client';

export const GET_BANNERS_SLIDER = gql`
    query getBannerSlider($sliderId: Int!) {
        lofBannerSlider(sliderId: $sliderId) {
            banners {
                link
                resource_path
                resource_type
                resource_map {
                    max_width
                    min_width
                }
                title
            }
        }
    }
`

export const GET_STORE_CONFIG_BANNER_SLIDER = gql`
    query storeConfigData {
        storeConfig {
            id
            code
            locale
            secure_base_media_url
            base_media_url
            copyright
            lof_bannerslider{
                enabled
                default_homepage_slider
                default_mobile_slider
                items_to_show
                sliding_speed
                autoplay
                autoplay_speed
                animation_style
                show_nav
                show_dots
                full_width
            }
        }
    }
`;